import dataclasses
import json
from collections.abc import Iterable
from datetime import datetime
from enum import Enum
from typing import Any

from rich.table import Table


def get_dataclass_columns(dc: Any) -> list[str]:
    return [field.name.title().replace("_", " ") for field in dataclasses.fields(dc)]


def render_table_row(*renderables: Any, enum_values: bool = False) -> list[str]:
    row = []
    for item in renderables:
        if item is None:
            row.append(":no_entry_sign:")
        elif isinstance(item, Enum):
            if enum_values:
                row.append(item.value)
            else:
                row.append(str(item))
        elif isinstance(item, bool):
            if item:
                row.append("[green]:heavy_check_mark:[/green]")
            else:
                row.append("[bold red]:x:[/bold red]")
        elif isinstance(item, datetime):
            row.append(item.strftime("%Y-%m-%d %T %Z"))
        elif isinstance(item, dict):
            row.append(json.dumps(item))
        else:
            row.append(str(item))
    return row


def from_dataclasses(dc: Any, rows: Iterable[Any]) -> Table:
    """
    Create a Table instance from dataclasses by passing in the dataclass and then iterables of
    instances of that dataclass.

    :param dc: The dataclass
    :param row: Iterable of instances of the dataclass.
    :return: Table with columns of fields of the dataclass.
    """
    table = Table(*get_dataclass_columns(dc))
    for row in rows:
        table.add_row(*render_table_row(*dataclasses.astuple(row)))
    return table
