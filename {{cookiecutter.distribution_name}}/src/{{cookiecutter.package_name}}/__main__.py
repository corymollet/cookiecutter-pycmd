"""
Main entrypoint module, i.e. `python -m{{cookiecutter.package_name}}`.
"""

import sys


if __name__ == "__main__":
    sys.stdout.write("Hello, world!\n")
