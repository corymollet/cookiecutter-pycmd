"""
Simple wrapper for running OS commands via subprocess
"""

import json
import subprocess
from dataclasses import dataclass, field
from typing import Any


@dataclass
class RunCmdResult:
    is_error: bool = False
    stdout: str = ""
    stderr: str = ""
    returncode: int = 0
    cmd: str = ""
    args: str | list[str] = field(default_factory=list)

    def stdout_json(self) -> Any:
        try:
            return json.loads(self.stdout)
        except json.JSONDecodeError:
            return None


def run_cmd(cmd: list[Any], exit_on_error=False, **kwargs) -> RunCmdResult:
    """
    Run a command on the host operating system via ``subprocess.run``

    :param cmd: The command to run, as a list of tokens (e.g. ``["ls", "-l"]``)
    :param exit_on_error: Whether to exit this program with a non-zero code if the called command returns a non-zero exit code.
    :param kwargs: Any additional keyword arguments to pass to ``subprocess.run``
    """
    cmd = [str(x) for x in cmd]
    result = RunCmdResult(cmd=" ".join(cmd))
    run = subprocess.run(cmd, check=False, capture_output=True, encoding="utf-8", **kwargs)
    result.stdout = run.stdout.strip()
    result.stderr = run.stderr.strip()
    result.returncode = run.returncode
    result.args = run.args
    result.is_error = result.returncode != 0
    if result.is_error and exit_on_error:
        raise SystemExit(result.stderr)
    return result
