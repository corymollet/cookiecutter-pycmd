README
######

This is a `Cookiecutter <https://cookiecutter.io>`_ template for a Python wrapper around other commands. It includes the `rich <https://rich.readthedocs.io>`_ for richly-formatted console output and `Textual <https://textual.textualize.io>`_ for creating terminal user interfaces if desired.

